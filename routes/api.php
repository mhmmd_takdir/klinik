<?php

use Illuminate\Http\Request;
use App\Http\Controllers\API\PasienController;
use App\Http\Controllers\API\RmController;
use App\Http\Controllers\API\DokterController;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\StafController;
use App\Http\Controllers\API\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/login', [ AuthController::class, 'login']);
Route::group(['middleware' => 'auth:sanctum'], function () {
    
    Route::post('/logout', [ AuthController::class, 'logout']);
    
    //pasien
    // Route::resource('pasiens',API\PasienController::class);
    
    Route::get('/pasiens', [ PasienController::class, 'index']);
    Route::post('/pasiens', [ PasienController::class, 'store']);
    Route::put('/pasiens/edit/{id}', [ PasienController::class, 'update']);
    Route::get('/pasiens/{id}', [ PasienController::class, 'show']);
    Route::delete('/pasiens/delete/{id}', [ PasienController::class, 'destroy']);


    //rm
    // Route::resource('rm',API\RmController::class);
    Route::get('/rm', [ RmController::class, 'index']);
    Route::post('/rm', [ RmController::class, 'store']);
    Route::put('/rm/edit/{id}', [ RmController::class, 'update']);
    Route::get('/rm/{id}', [ RmController::class, 'show']);
    Route::delete('/rm/delete/{id}', [ RmController::class, 'destroy']);

    //ambi data rm berdasarkan id pasien
    Route::get('/rm/list_pasien/{idpasien}', [ RmController::class, 'list_pasien']);
    
    
    //user
    // Route::resource('user',API\UserController::class);
    Route::get('/user', [ UserController::class, 'index']);
    Route::post('/user', [ UserController::class, 'store']);
    Route::put('/user/edit/{id}', [ UserController::class, 'update']);
    Route::get('/user/{id}', [ UserController::class, 'show']);
    Route::delete('/user/delete/{id}', [ UserController::class, 'destroy']);

    // dokter
    // Route::resource('dokter',API\DokterController::class);
    Route::get('/dokter', [ DokterController::class, 'index']);
    Route::post('/dokter', [ DokterController::class, 'store']);
    Route::put('/dokter/edit/{id}', [ DokterController::class, 'update']);
    Route::get('/dokter/{id}', [ DokterController::class, 'show']);
    Route::delete('/dokter/delete/{id}', [ DokterController::class, 'destroy']);

    //Staf
    Route::get('/staf', [ StafController::class, 'index']);
    Route::post('/staf', [ StafController::class, 'store']);
    Route::put('/staf/edit/{id}', [ StafController::class, 'update']);
    Route::get('/staf/{id}', [ StafController::class, 'show']);
    Route::delete('/staf/delete/{id}', [ StafController::class, 'destroy']);
});



