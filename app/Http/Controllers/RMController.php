<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Auth;

class RMController extends Controller
{
    public function index() {
        $rms = ambil_semuadata('rm');
        $metadatas = ambil_satudata('metadata',12);
        return view('rm', compact('rms','metadatas'));
    }
    public function hapus_rm($id)
    {
        DB::table('rm')->where('id',$id)->update([
            'deleted' => 1,
        ]);
        $pesan="Data pasien berhasil dihapus!";
        return redirect ('rm')->with('pesan', $pesan);
    }
    public function update_rm(Request $request)
    {
        $this->validate($request, [
            'idpasien' => 'required|numeric|digits_between:1,4',
            'keluhan_utama' => 'required|max:40',
            'gigi' => 'required|max:20',
            'diagnosis' => 'max:40',
            'dokter' => 'required',
            'konsul' => 'nullable',
            'status' => 'nullable',
        ]);
        
        DB::table('rm')->where('id',$request->id)->update([
            'idpasien' => $request->idpasien,
            'ku' => $request->keluhan_utama,
            'gigi' => $request->gigi,
            'diagnosis' => $request->diagnosis,
            'dokter' => $request->dokter,
            'konsul' => $request->konsul,
            'status' => $request->status,
            'updated_time' => Carbon::now(),
        ]);
           
        return redirect('rm')->with(['pesan' => 'Data pasien berhasil update']);     
        
    }
    //Hallaman Edit Pasien
    public function edit_rm($id)
    {
        if (Auth::User()->admin !== 1) {
            if (Auth::User()->profesi !== "Dokter") {
                abort(403, 'Anda Tidak berhak Mengakses Halaman Ini.');
            }
            $dokters=DB::table('rm')->select('dokter')->where('id',$id)->get();;
            foreach ($dokters as $dokter) {            
                if (Auth::User()->id !== $dokter->dokter) {
                abort(403, 'Anda Tidak berhak Mengakses Halaman Ini.');
                }
            }
        }
        
        $metadatas = ambil_satudata('metadata',13);
        $datas= ambil_satudata('rm',$id);
        if ($datas->count() <= 0) {
            return abort(404, 'Halaman Tidak Ditemukan.');
        }
        foreach ($datas as $data) {
            //mencari id pasien dari id RM
             if ($data->idpasien != NULL) {$idpasien = $data->idpasien;  $idens=DB::table('pasien')->where('id',$idpasien)->get();}
        }
        $dokters = DB::table('users')->where('profesi','Dokter')->get();
      return view('edit-rm',compact('metadatas','idens','datas','dokters'));
    }
    
    public function list_rm($idpasien)
    {
        $metadatas = ambil_satudata('metadata',12);
        $idens = ambil_satudata('pasien',$idpasien);
        if ($idens->count() <= 0) {
            return abort(404, 'Halaman Tidak Ditemukan.');
        }
        $rms = DB::table('rm')->where('idpasien',$idpasien)->get();

        return view('list-rm',compact('metadatas','idens','rms'));

    }
    
    public function tambah_rm()
    {
        $metadatas = ambil_satudata('metadata',11);
        $pasiens = ambil_semuadata('pasien');
        $cont=[
          'aria'=>'true',
          'show'=>'show',
          'col'=>''  
        ];
        return view('tambah-rm',compact('metadatas','pasiens','cont'));  
    }
    
        public function tambah_rmid($idpasien)
    {
        $metadatas = ambil_satudata('metadata',11);
        $pasiens = ambil_semuadata('pasien');
        $idens = ambil_satudata('pasien',$idpasien);
        $dokters = DB::table('users')->where('profesi','Dokter')->get();
        $cont=[
          'aria'=>'false',
          'show'=>'',
          'col'=>'collapsed'  
        ];

        return view('tambah-rm',compact('metadatas','idens','pasiens','cont','dokters','pasiens'));  
    }
    
           public function simpan_rm(Request $request)
    {  

        $this->validate($request, [
            'idpasien' => 'required|numeric|digits_between:1,4',
            'keluhan_utama' => 'required|max:40',
            'gigi' => 'required|max:20',
            'diagnosis' => 'max:40',
            'dokter' => 'required',
            // 'konsul' => 'required',
        ]);
   
        DB::table('rm')->insert([
            'idpasien' => $request->idpasien,
            'ku' => $request->keluhan_utama,
            'gigi' => $request->gigi,
            'diagnosis' => $request->diagnosis,
            'dokter' => $request->dokter,
            'konsul' => $request->konsul,
            'created_time' => Carbon::now(),
            'updated_time' => Carbon::now(),
        ]);
           $ids= DB::table('rm')->latest('created_time')->first();         
        return redirect('rm')->with(['pesan' => 'Data pasien berhasil disimpan']);
         
    }
    
        public function lihat_rm($id)
    {
        $metadatas = ambil_satudata('metadata',15);
        $datas= ambil_satudata('rm',$id);
        if ($datas->count() <= 0) {
            return abort(404, 'Halaman Tidak Ditemukan.');
        }
        foreach ($datas as $data) {
            //mencari id pasien dari id RM
             $idpasien = $data->idpasien; 
        }
        $idens=DB::table('pasien')->where('id',$idpasien)->get();
      return view('lihat-rm',compact('metadatas','idens','datas'));

    }
}