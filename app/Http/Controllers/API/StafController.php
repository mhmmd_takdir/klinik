<?php

namespace App\Http\Controllers\API;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StafController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staf = User::where('profesi','Staff')->get();
        return response()->json($staf);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'username' => 'required|min:5|max:255|unique:users',
            // 'profesi' => 'required',
            'password' => 'required|string|min:8',
            'name' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'admin' => 'required|boolean',
        ]);        
        $validateData['profesi']= "Staff";
        $validateData['password'] = Hash::make($validateData['password']);
        try {
           $respon = User::create($validateData);
           return response()->json([
               'success' => true,
               'message' => 'success',
               "data"=> $respon
           ]);
        } catch (Exception $e) {
            return response()->json([
                "message" => "inputan gagal ",
                "errors" => $e->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $staf = User::where('profesi','Staff')->where('id',$id)->first();
            if (is_null($staf)) {
                return response()->json('Data not found', 404); 
            }
            return response()->json($staf);   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = $request->validate([
            'username' => 'required|min:5|max:255|unique:users',
            // 'profesi' => 'required',
            'password' => 'required|string|min:8',
            'name' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'admin' => 'required|boolean',
        ]);      

        try {
            User::where('id', $id)->where('profesi', 'Staff')->update($validateData);
            $respon=User::where('id', $id)->where('profesi', 'Staff')->first();
            return response()->json([
                'success' => true,
                'message' => 'success',
                'data' => $respon
            ]);
         } catch (Exception $e) {
             return response()->json([
                 "message" => "inputan gagal ",
                 "errors" => $e->getMessage()
             ]);
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $staf = User::find($id);
        $staf->delete();

        return response()->json('Program deleted successfully');
    }
}
