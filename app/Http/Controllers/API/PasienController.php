<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use App\Models\Pasien;
use Validator;


class PasienController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $pasien = ambil_semuadata('pasien');
        $pasien = Pasien::all();
        return response()->json($pasien);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'nama' => 'required|min:5|max:35',
            'id_user' => 'required',
            'tgl_lhr' => 'required|before:today',
            'alamat' => 'required',
            'pekerjaan' => 'required',
            'hp' => 'required|numeric',
            'jk' => 'required',
            'no_bpjs' => 'nullable|numeric|digits_between:1,15'
        ]);
        $validateData['created_time'] = Carbon::now();
        $validateData['updated_time'] = Carbon::now();

        try {
           $respon = Pasien::create($validateData);
           return response()->json([
               'success' => true,
               'message' => 'success',
               "data"=> $respon
           ]);
        } catch (Exception $e) {
            return response()->json([
                "message" => "inputan gagal ",
                "errors" => $e->getMessage()
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pasien = Pasien::where('id',$id)->first();
        if (is_null($pasien)) {
            return response()->json('Data not found', 404); 
        }
        return response()->json($pasien);  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = $request->validate([
            'nama' => 'required|min:5|max:35',
            'id_user' => 'required',
            'tgl_lhr' => 'required|before:today',
            'alamat' => 'required',
            'pekerjaan' => 'required',
            'hp' => 'required|numeric',
            'jk' => 'required',
            'no_bpjs' => 'nullable|numeric|digits_between:1,15'
        ]);
        $validateData['updated_time'] = Carbon::now();

            try {
                $respon = Pasien::where('id', $id)->update($validateData);
                return response()->json([
                    'success' => true,
                    'message' => 'success',
                    'data' => $respon
                ]);
             } catch (Exception $e) {
                 return response()->json([
                     "message" => "inputan gagal ",
                     "errors" => $e->getMessage()
                 ]);
             }
     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try {
            $pasien = Pasien::find($id);
            $pasien->delete();
            return response()->json([
                'success' => true,
                'message' => 'data dihapus'
            ]);
         } catch (Exception $e) {
             return response()->json([
                 "message" => "pasien tidak ditemukan",
                 "errors" => $e->getMessage()
             ]);
         }
    }
}
