<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use App\Models\Rm;
use Validator;


class RmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rm = Rm::all();
        // $rm = Rm::with('user')->get();
        return response()->json($rm);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'idpasien' => 'required|numeric|digits_between:1,4',
            'ku' => 'required|max:40',
            'gigi' => 'required|max:20',
            'diagnosis' => 'max:40',
            'dokter' => 'required',
            'konsul' => 'required',
            'status' => 'required',
        ]);
        $validateData['created_time'] = Carbon::now();
        $validateData['updated_time'] = Carbon::now();

        try {
           $respon = Rm::create($validateData);
           return response()->json([
               'success' => true,
               'message' => 'success',
               "data"=> $respon
           ]);
        } catch (Exception $e) {
            return response()->json([
                "message" => "inputan gagal ",
                "errors" => $e->getMessage()
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rm = Rm::where('id',$id)->first();
        if (is_null($rm)) {
            return response()->json('Data not found', 404); 
        }
        return response()->json($rm); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = $request->validate([
            'idpasien' => 'required|numeric|digits_between:1,4',
            'ku' => 'required|max:40',
            'gigi' => 'required|max:20',
            'diagnosis' => 'max:40',
            'dokter' => 'required',
            'konsul' => 'required',
            'status' => 'required',
            
        ]);
        $validateData['updated_time'] = Carbon::now();
        

            try {
                $respon = Rm::where('id', $id)->update($validateData);
                return response()->json([
                    'success' => true,
                    'message' => 'success'
                ]);
             } catch (Exception $e) {
                 return response()->json([
                     "message" => "inputan gagal ",
                     "errors" => $e->getMessage()
                 ]);
             }
     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try {
            $rm = Rm::find($id);
            $rm->delete();
            return response()->json([
                'success' => true,
                'message' => 'data dihapus'
            ]);
         } catch (Exception $e) {
             return response()->json([
                 "message" => "inputan gagal ",
                 "errors" => $e->getMessage()
             ]);
         }
    }

    public function list_pasien($idpasien)
    {
        $rm = Rm::where('idpasien',$idpasien)->get();
        if (is_null($rm)) {
            return response()->json('Data not found', 404); 
        }
        return response()->json($rm); 
    }
}
