<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use App\Models\User;
use Validator;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $pasien = ambil_semuadata('pasien');
        $user = User::all();
        return response()->json($user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'username' => 'required|min:5|max:255|unique:users',
            'profesi' => 'required',
            'password' => 'required|string|min:8',
            'name' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'admin' => 'required|boolean',
        ]);        
        $validateData['password'] = Hash::make($validateData['password']);
        try {
           $respon = User::create($validateData);
           return response()->json([
               'success' => true,
               'message' => 'success',
               "data"=> $respon
           ]);
        } catch (Exception $e) {
            return response()->json([
                "message" => "inputan gagal ",
                "errors" => $e->getMessage()
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::where('id',$id)->first();
        if (is_null($user)) {
            return response()->json('Data not found', 404); 
        }
        return response()->json($user); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = $request->validate([
            'username' => 'required|min:5|max:255|unique:users',
            'profesi' => 'required',
            'password' => 'required|string|min:8',
            'name' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'admin' => 'required|boolean',
        ]);
        $validateData['password'] = Hash::make($validateData['password']);

            try {
                $respon = User::where('id', $id)->update($validateData);
                return response()->json([
                    'success' => true,
                    'message' => 'success'
                ]);
             } catch (Exception $e) {
                 return response()->json([
                     "message" => "inputan gagal ",
                     "errors" => $e->getMessage()
                 ]);
             }
     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try {
            $user = User::find($id);
            $user->delete();
            return response()->json([
                'success' => true,
                'message' => 'data dihapus'
            ]);
         } catch (Exception $e) {
             return response()->json([
                 "message" => "inputan gagal ",
                 "errors" => $e->getMessage()
             ]);
         }
    }
}
