<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rm extends Model
{
    protected $table = 'rm';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'idpasien', 'id');
    }
    public function pasien()
    {
        return $this->belongsTo(Pasien::class, 'idpasien', 'id');
    }
    public function dokter()
    {
        return $this->belongsTo(User::class, 'dokter', 'id');
    }
}
